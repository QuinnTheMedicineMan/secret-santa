#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name='secret-santa',
    version='0.1',
    description='My overengineered solution to the annual secret santa',
    author='Liam Quinn',
    packages=setuptools.find_packages()
)
