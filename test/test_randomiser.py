#!/usr/bin/env python3

from secret_santa.randomiser import Randomiser


TEST_LIST = [
    ('Tom', 'foo'),
    ('Dick', 'bar'),
    ('Harry', 'baz'),
]


def test_randomiser_output_length():

    hat = Randomiser(TEST_LIST)
    x = hat.randomise()

    assert(len(x) == 3)


def test_randomiser_output_no_missing():

    hat = Randomiser(TEST_LIST)
    x = hat.randomise()

    assert('Tom' in x.keys()
       and 'Dick' in x.keys()
       and 'Harry' in x.keys())
    assert(('Tom', 'foo') in x.values()
       and ('Dick', 'bar') in x.values()
       and ('Harry', 'baz') in x.values())


def test_randomiser_no_presents_for_self():

    hat = Randomiser(TEST_LIST)
    x = hat.randomise()

    for key in x.keys():
        assert(x[key][0] != key)
