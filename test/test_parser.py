#!/usr/bin/env python3

# Standard Library imports
import os

# External imports
import pytest

# Local imports
from secret_santa.app import _parse_guest_list
import secret_santa.exceptions


def test_parser_guestlist():
    '''Read an input guest list file and verify the contents.'''
    guestlist = _parse_guest_list(os.path.join('test', 'guestlist1.txt'))
    assert guestlist == [
        ('Tom', '123 Fake Street'),
        ('Dick', '10 Downing Street'),
        ('Harry', '999 The Moon'),
    ]


def test_parser_empty():
    '''Make sure the parser complains if given an empty file.'''
    with pytest.raises(secret_santa.exceptions.ParserException):
        guestlist = _parse_guest_list(os.path.join('test', 'guestlist2.txt'))

def test_duplicates_disallowed():
    '''Make sure the parser complains if there are duplicate names.'''
    with pytest.raises(secret_santa.exceptions.ParserException):
        guestlist = _parse_guest_list(os.path.join('test', 'guestlist3.txt'))
