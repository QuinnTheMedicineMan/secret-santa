#!/usr/bin/env python3

"""Exceptions used in the secret santa randomiser."""


class BaseException(Exception):
    """Base class all other exceptions are derived from.
    Shouldn't be used.
    """
    pass


class ParserException(BaseException):
    """Raised when unable to parse the guest list."""
    pass


class RandomiserException(BaseException):
    """Raised when the randomiser class does something unexpected."""
    pass
