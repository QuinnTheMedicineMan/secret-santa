#!/usr/bin/env python3

# Standard Library imports
import argparse
import logging


# Local imports
from . import exceptions
from . import randomiser


# Code and classes
def _parse_guest_list(inputFile_):
    """Reads a guest list text file and converts it into a list.

    The input file should contain no duplicate names and each guest
    should be on a separate line.

    Args:
        inputFile_ (str): The guest list text file.

    Returns:
        (list of str): The list of guests
    """
    output = []
    with open(inputFile_) as inputFile:
        for line in inputFile:
            name = line.split(',')[0]
            address = ','.join(line.split(',')[1:]).strip('\n').lstrip()
            if name != '':
                output.append((name, address))

    if len(output) == 0:
        raise exceptions.ParserException(
            'There are no guests!')

    if len(output) != len(set(output)):
        raise exceptions.ParserException(
            'There are duplicate names in the guest list.')

    logging.info('Reading guest list: {}'.format(output))

    return output


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--inputFile', type=str, required=True)
    parser.add_argument('-d', '--debug', action='store_true', default=False)
    args = parser.parse_args()

    logging.basicConfig()

    if args.debug:
        logging.getLogger().setLevel(logging.INFO)

    guests = _parse_guest_list(args.inputFile)

    hat = randomiser.Randomiser(guests)
    output = hat.randomise()

    for x, y in output.items():
        logging.info('{} -> {}'.format(x, y))

    if not args.debug:
        for giver, (getter, address) in output.items():
            with open('{}.txt'.format(giver), 'w') as f:
                f.write(
                    '{}, you should buy a present for {} at {}.'.format(
                        giver,
                        getter,
                        address,
                    )
                )
