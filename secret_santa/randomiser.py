#!/usr/bin/env python3

# Standard Library imports
import random
import time

# Local imports
from . import exceptions


class Randomiser:
    """Class to handle the picking of names out of a hat."""

    def __init__(self, guests):
        """Constructor

        Args:
            guests (list of str): A list of people participating in the
                secret santa
        """
        if len(guests) < 2:
            raise exceptions.RandomiserException(
                'You need at least two guests.')
        if len(guests) != len(set(guests)):
            raise exceptions.RandomiserException(
                'There is a duplicate name in guestlist: {}'.format(guests))

        random.seed(time.time())

        self._guests = guests

    def randomise(self):
        """Assign each participant a guest to buy a present for.

        Returns:
            dict of (str: str) : randomised dictionary of format
              {present getter : present receiver}
        """
        random.shuffle(self._guests)

        output = {}
        for i, (guest, address) in enumerate(self._guests):
            try:
                output[guest] = self._guests[i + 1]
            except IndexError:
                output[guest] = self._guests[0]
        return output
